package com.example.demoRestService.services;

import com.example.demoRestService.web.model.CarRec;
import org.springframework.stereotype.Service;

@Service
public class CarServiceImpl implements CarService {

    @Override
    public CarRec getCarByID ( int carId ) {

        return CarRec.builder ( ).carId( 1 )
                                 .carBrand("Nissan")
                                 .carModel("Sentra")
                                 .carColor("Silver")
                                 .carYear("2018").build ( );

    }
}
