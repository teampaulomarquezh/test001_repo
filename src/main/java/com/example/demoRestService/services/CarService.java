package com.example.demoRestService.services;

import com.example.demoRestService.web.model.CarRec;

public interface CarService {

    CarRec getCarByID ( int carId );

}
