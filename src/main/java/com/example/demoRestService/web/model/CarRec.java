package com.example.demoRestService.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarRec {
    int carId;
    String carBrand;
    String carModel;
    String carYear;
    String carColor;
}
