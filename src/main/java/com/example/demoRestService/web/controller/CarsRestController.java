package com.example.demoRestService.web.controller;

import com.example.demoRestService.services.CarService;
import com.example.demoRestService.web.model.CarRec;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping ( "/DocIndexerAPI/1.0/claims" )
@RestController
public class CarsRestController {

    private final CarService carService;

    public CarsRestController ( CarService carService ) { this.carService = carService; }

    @GetMapping({"/getCarById/{carId}"})
    public ResponseEntity<CarRec> getCarById (@PathVariable("carId") int carId ) {

        return new ResponseEntity<> ( carService.getCarByID ( carId ), HttpStatus.OK );
    }
}
